#include "util/dyna.h"

#include <stddef.h>
#include <string.h>

typedef struct {
  size_t member_size;
  size_t n_elements;
  size_t capacity;

  /// the purpose of this field is align any data format
  //  that will come after the header
  char aligment[sizeof(size_t[3]) % alignof(max_align_t)];
} dyna_header_t;

static dyna_header_t* dyna_get_header(void* ptr)
{
  return  ((dyna_header_t*) ptr) - 1;
}

static dyna_header_t const* dyna_get_const_header(void const* ptr)
{
  return  ((dyna_header_t*) ptr) - 1;
}

static void* dyna_get_array(dyna_header_t* ptr)
{
  return ptr + 1;
}

void* dyna_create_implementation(size_t member_size, size_t initial_capacity)
{
  if(initial_capacity < 2)
    initial_capacity = 2;

  dyna_header_t* dyna = (dyna_header_t*) malloc(
    sizeof(dyna_header_t) + member_size * initial_capacity
  );

  dyna->member_size = member_size;
  dyna->n_elements = 0;
  dyna->capacity = initial_capacity;

  return dyna_get_array(dyna);
}

void dyna_destroy(void* dyna)
{
  free(dyna_get_header(dyna));
}

void* dyna_ptr_at(void* dyna, size_t idx)
{
  size_t ms = dyna_get_header(dyna)->member_size;
  return (void*)((char*) dyna + (idx * ms));
}

void* dyna_get_last(void* dyna)
{
  size_t n = dyna_size(dyna);
  if(n <= 1) return dyna;
  return dyna_ptr_at(dyna, n - 1);
}

void* dyna_resize(void* dyna, size_t new_capacity)
{
  dyna_header_t* header = dyna_get_header(dyna);
  header = realloc(
    header,
    sizeof(dyna_header_t) + new_capacity * header->member_size
  );

  header->capacity = new_capacity;
  if(header->capacity < header->n_elements)
    header->n_elements = header->capacity;

  return dyna_get_array(header);
}

void dyna_set_at(void* dyna, size_t idx, void* elem)
{
  memcpy(dyna_ptr_at(dyna, idx), elem, dyna_get_header(dyna)->member_size);
}

void* dyna_add(void* dyna, void* elem)
{
  size_t newsiz = dyna_get_header(dyna)->n_elements + 1;

  if(newsiz >= dyna_get_header(dyna)->capacity)
    dyna = dyna_resize(dyna, dyna_get_header(dyna)->capacity*2);

  dyna_set_at(dyna, dyna_get_header(dyna)->n_elements, elem);
  dyna_get_header(dyna)->n_elements++;

  return dyna;
}

size_t dyna_size(void const* dyna)
{
  return dyna_get_const_header(dyna)->n_elements;
}

size_t dyna_capacity(void const* dyna)
{
  return dyna_get_const_header(dyna)->capacity;
}
