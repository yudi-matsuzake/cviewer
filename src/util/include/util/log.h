#ifndef __LOG_H__
#define __LOG_H__

#include <libgen.h>

#define fatal_error(...)           \
  do{                              \
    fprintf(stderr, __VA_ARGS__);  \
    glfwTerminate();               \
    exit(EXIT_FAILURE);            \
  }while(0)

#ifdef NDEBUG
#define log_inf(...) do{}while(0)
#define log_err(...) do{}while(0)
#define log_war(...) do{}while(0)
#else
#define log_gen(prefix, file, func, line, ...) \
  do{                                    \
    fprintf(                             \
      stderr,                            \
      "[%s] %s(%d) at %s(): ",           \
      prefix,                            \
      basename(file),                    \
      line,                              \
      func                               \
    );                                   \
    fprintf(stderr, __VA_ARGS__);        \
  }while(0)

#define log_inf(...) log_gen("inf", __FILE__, __func__, __LINE__, __VA_ARGS__)
#define log_err(...) log_gen("err", __FILE__, __func__, __LINE__, __VA_ARGS__)
#define log_war(...) log_gen("war", __FILE__, __func__, __LINE__, __VA_ARGS__)
#endif // NDEBUG

#endif
