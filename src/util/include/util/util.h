#ifndef __UTIL_H__
#define __UTIL_H__

#include <stddef.h>
#include <stdio.h>
#include <ctype.h>

#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

/**
  * computes whether `a` ends with `b`
  */
int endswith(char const* a, char const* b);

/**
  * computes whether `a` begins with `b`
  */
int beginswith(char const* a, char const* b);

/**
  * appends the buffer `src` with size `src_siz` bytes to the 
  * buffer `dst` with `dst_siz` that was created with `malloc`
  *
  * if `dst` has no capacity to accommodate `src_siz` bytes,
  * `realloc` function will allocate it
  *
  * @return new pointer containing `dst`
  */
void* append_to_buf(void* dst, size_t dst_siz, void* src, size_t src_siz);

/**
  * calls fread with these arguments but return > 0 if
  * was a successful reading
  */
int read_ok(void* ptr, size_t size, size_t nmemb, FILE* in);

/**
  * return the next char that is not a digit, that is not ([0-9]|[+-])
  */
char* next_non_digit(char* str);

/**
  * return the next char that is a digit, that is ([0-9]|[+-])
  */
char* next_digit(char* str);

#endif
