#ifndef __DYNA_H__
#define __DYNA_H__

#include <stdlib.h>
#include <stdalign.h>

/**
  * @file dyna.h defines and describes dyna, *dyn*amic *a*rray routines
  *
  * a dyna is a dynamic array that may increase the size as needed
  *
  * dyna is allocated using realloc, if needs more capacity, it will
  * automatically allocates more space
  *
  * the pointer will be always valid array, and the metadata is stored
  * before the pointer of the array
  *
  * the memory layout is be something like that
  *
  *  +--------+-------------------------------+
  *  | header | array data                    |
  *  +--------+-------------------------------+
  *           |
  *           `-> pointer returned to the user.
  *
  * therefore, this could be used as a regular array:
  *
  * int* a = dyna_create(int, 10);
  *
  * for(int i=0; i<10; ++i)
  *    a = dyna_add(a, &i);
  * for(int i=0; i<10; ++i)
  *   printf("%d\n", a[i]);
  *
  */

/*
 * creates an array with initial capacity `initial capacity`
 * with members of size `member_size`
 *
 * @param member_size is the size of every element in dyna array; for instance,
 *        for ints will be sizeof(int)
 *
 * @param initial_capacity is the initial capacity of the container
 */
void* dyna_create_implementation(
    size_t member_size, size_t initial_capacity);

#define dyna_create(TYPE, initial_capacity)           \
  (TYPE*) dyna_create_implementation(sizeof(TYPE), initial_capacity)

/*
 * destroys a dyna array
 */
void dyna_destroy(void* dyna);

/*
 * get the pointer of index `idx`
 */
void* dyna_ptr_at(void* dyna, size_t idx);
void* dyna_get_last(void* dyna);

/*
 * push an value at the last position of the array
 */
void* dyna_add(void* dyna, void* elem);
void* dyna_resize(void* dyna, size_t new_capacity);

void dyna_set_at(void* dyna, size_t idx, void* elem);

/*
 * returns the number of elements in the array
 */
size_t dyna_size(void const* dyna);

/*
 * returns the capacity of elements that fits on the current allocated memory
 */
size_t dyna_capacity(void const* dyna);

#endif
