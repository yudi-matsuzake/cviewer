#include "util/util.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

int endswith(char const* a, char const* b)
{
  uint64_t as = strlen(a);
  uint64_t bs = strlen(b);

  if(bs > as)
    return 0;

  if(bs == 0)
    return 1;

  if(a == 0){
    if(a == b)
      return 1;
    return 0;
  }

  return strcmp(a + (as - bs), b) == 0;
}

int beginswith(char const* a, char const* b)
{
  while(*a != '\0' && *b != '\0' && *a == *b){
    ++a;
    ++b;
  };
  return *b == '\0';
}

void* append_to_buf(void* dst, size_t dst_siz, void* src, size_t src_siz)
{
  dst = realloc(dst, dst_siz + src_siz);
  memcpy((char*)dst + dst_siz, src, src_siz);
  return dst;
}

int read_ok(void* restrict ptr, size_t size, size_t nmemb, FILE* in)
{
  size_t n_read = fread(ptr, size, nmemb, in);
  return n_read == nmemb;
}

char* next_non_digit(char* str)
{
  if(str){
    while(*str == '+' || *str == '-' || isdigit(*str)){
      str++;
    }
    return str;
  }
  return NULL;
}

char* next_digit(char* str)
{
  if(str){
    while(*str != '+'
      && *str != '-'
      && !isdigit(*str)
      && *str != '\0'){

      str++;
    }
    return str;
  }
  return NULL;
}

