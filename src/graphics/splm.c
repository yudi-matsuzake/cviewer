#include "graphics/splm.h"

static const char* splm_vs =
"#version 330 core\n"
"layout (location = 0) in vec3 l_pos;\n"
"layout (location = 1) in vec3 l_normal;\n"

"out vec3 frag_position;\n"
"out vec3 normal;\n"

"uniform mat4 u_model;\n"
"uniform mat4 u_view;\n"
"uniform mat4 u_projection;\n"

"void main()\n"
"{\n"
"  frag_position = vec3(u_model * vec4(l_pos, 1.0));\n"
"  normal = mat3(transpose(inverse(u_model))) * l_normal;  \n"

"  gl_Position = u_projection * u_view * vec4(frag_position, 1.0);\n"
"}\n";


static const char* splm_fs =
"#version 330 core\n"
"out vec4 FragColor;\n"

"in vec3 normal;\n"
"in vec3 frag_position;\n"

"uniform vec3 u_light_position;\n"
"uniform vec3 u_view_position;\n"
"uniform vec3 u_light_color;\n"
"uniform vec3 u_object_color;\n"
"uniform float u_ambient_light_strength;\n"

"void main()\n"
"{\n"
"  // ambient\n"
"  vec3 ambient = u_ambient_light_strength * u_light_color;\n"

"  // diffuse \n"
"  vec3 norm = normalize(normal);\n"
"  vec3 light_direction = normalize(u_light_position - frag_position);\n"
"  float diff = max(dot(norm, light_direction), 0.0);\n"
"  vec3 diffuse = diff * u_light_color;\n"

"  // specular\n"
"  float specularStrength = 0.5;\n"
"  vec3 viewDir = normalize(u_view_position - frag_position);\n"
"  vec3 reflectDir = reflect(-light_direction, norm);  \n"
"  float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);\n"
"  vec3 specular = specularStrength * spec * u_light_color;  \n"

"  vec3 result = (ambient + diffuse + specular) * u_object_color;\n"
"  FragColor = vec4(result, 1.0);\n"
"} \n";


void splm_create(splm_t* p)
{
  p->shader = shader_create(
    &((shader_source_t){ &splm_vs, 1 }),
    &((shader_source_t){ &splm_fs, 1 })
  );
  p->ambient_light_strength = 0.1f;
}

void splm_destroy(splm_t* s)
{
  shader_destroy(s->shader);
}

void splm_use(
  splm_t const* p,
  scene_t const* scene,
  mat4s const* projection,
  mat4s const* view,
  mat4s const* model,
  vec3s const* color)
{
  shader_use(p->shader);

  shader_set_uniform_vec3(p->shader, "u_object_color", color);
  shader_set_uniform_vec3(p->shader, "u_light_color", &scene->light.color);
  shader_set_uniform_vec3(p->shader, "u_light_position", &scene->light.position);
  shader_set_uniform_vec3(p->shader, "u_view_position", &scene->camera.position);
  shader_set_uniform_mat4(p->shader, "u_projection", projection);
  shader_set_uniform_mat4(p->shader, "u_view", view);
  shader_set_uniform_mat4(p->shader, "u_model", model);
  shader_set_uniform_float(
    p->shader,
    "u_ambient_light_strength",
    p->ambient_light_strength
  );
}
