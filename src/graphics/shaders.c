#include "graphics/shaders.h"
#include "util/log.h"

static void check_shader_compilation(shader_t s)
{
  int32_t success = 0;
  GL(glGetShaderiv(s, GL_COMPILE_STATUS, &success));

  if(!success){
    int32_t infolog_length = 0;
    GL(glGetShaderiv(
      s,
      GL_INFO_LOG_LENGTH,
      &infolog_length
    ));

    char* data = (char*) malloc(sizeof(char) * (size_t) infolog_length);
    GL(glGetShaderInfoLog(s, infolog_length, NULL, data));
    log_err("shader compilation error: [ %s ]\n", data);
    free(data);
  }
}

static void check_program_linkage(uint32_t id)
{
  int32_t success = 0;
  GL(glGetProgramiv(id, GL_LINK_STATUS, &success));

  if(!success){
    int32_t infolog_length = 0;
    GL(glGetProgramiv(
      id,
      GL_INFO_LOG_LENGTH,
      &infolog_length
    ));

    char* data = (char*) malloc(sizeof(char) * (size_t) infolog_length);
    GL(glGetProgramInfoLog(id, infolog_length, NULL, data));
    log_err("program linkage error: [ %s ]\n", data);
    free(data);
  }
}

static void compile_shader(shader_t s)
{
  GL(glCompileShader(s));
  GL(check_shader_compilation(s));
}

shader_t shader_create(
    shader_source_t const* vs,
    shader_source_t const* fs)
{
  // create&compile vertex shader
  GLuint vertex_shader;
  GL(vertex_shader = glCreateShader(GL_VERTEX_SHADER));
  GL(glShaderSource(vertex_shader, vs->count, vs->sources, NULL));
  compile_shader(vertex_shader);

  // create&compile fragment shader
  GLuint fragment_shader;
  GL(fragment_shader = glCreateShader(GL_FRAGMENT_SHADER));
  GL(glShaderSource(fragment_shader, vs->count, fs->sources, NULL));
  compile_shader(fragment_shader);

  // create&link program
  GLuint program;
  GL(program = glCreateProgram());
  GL(glAttachShader(program, vertex_shader));
  GL(glAttachShader(program, fragment_shader));
  GL(glLinkProgram(program));
  check_program_linkage(program);

  // destroing shader objects
  GL(glDeleteShader(vertex_shader));
  GL(glDeleteShader(fragment_shader));

  return program;
}

void shader_destroy(shader_t s)
{
  GL(glDeleteProgram(s));
}

void shader_use(shader_t s)
{
  GL(glUseProgram(s));
}

void shader_set_uniform_vec2(shader_t s, char const* name, vec2s const* v)
{
  GLint location;
  GL(location = glGetUniformLocation(s, name));
  GL(glUniform2f(location, v->x, v->y));
}

void shader_set_uniform_vec3(shader_t s, char const* name, vec3s const* v)
{
  GLint location;
  GL(location = glGetUniformLocation(s, name));
  GL(glUniform3f(location, v->x, v->y, v->z));
}

void shader_set_uniform_vec4(shader_t s, char const* name, vec4s const* v)
{
  GLint location;
  GL(location = glGetUniformLocation(s, name));
  GL(glUniform4f(location, v->x, v->y, v->z, v->w));
}

void shader_set_uniform_mat4(shader_t s, char const* name, mat4s const* m)
{
  GLint location;
  GL(location = glGetUniformLocation(s, name));
  GL(glUniformMatrix4fv(location, 1, GL_FALSE, (float*) m->raw));
}

void shader_set_uniform_float(shader_t s, char const* name, float v)
{
  GLint location;
  GL(location = glGetUniformLocation(s, name));
  GL(glUniform1f(location, v));
}

void shader_set_uniform_ivec2(shader_t s, char const* name, ivec2s const* v)
{
  GLint location;
  GL(location = glGetUniformLocation(s, name));
  GL(glUniform2i(location, v->x, v->y));
}
