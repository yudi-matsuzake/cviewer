#include "graphics/opengl.h"

#include <stdio.h>
#include <assert.h>

void glerror_msg(
  int pre,
  const char* call,
  const char* file,
  long line,
  const char* function,
  const char* error_macro,
  const char* error_description)
{
  const char* when = pre?"before":"after";

  fprintf(
    stderr,
    "OpenGL ERROR ---------------------------------------------\n"
    "\t%s: %s\n"
    "\toccurred %s the call: %s\n"
    "\t%s (%ld) in function %s\n",
    error_macro, error_description,
    when, call,
    file, line,
    function
  );
}

void glerror_pre_callback(
  const char* call,
  const char* file,
  long line,
  const char* function,
  const char* error_macro,
  const char* error_description)
{

  assert(call != NULL &&
    file != NULL &&
    function != NULL &&
    error_macro != NULL &&
    error_description != NULL);
  glerror_msg(
    1,
    call,
    file,
    line,
    function,
    error_macro,
    error_description
  );
}

void glerror_post_callback(
  const char* call,
  const char* file,
  long line,
  const char* function,
  const char* error_macro,
  const char* error_description)
{
  assert(call != NULL &&
    file != NULL &&
    function != NULL &&
    error_macro != NULL &&
    error_description != NULL);
  glerror_msg(
    0,
    call,
    file,
    line,
    function,
    error_macro,
    error_description
  );
}

const char* glerror_get_error_macro(GLenum error)
{
  switch(error){
  case GL_NO_ERROR:
    return "GL_NO_ERROR";
  case GL_INVALID_ENUM:
    return "GL_INVALID_ENUM";
  case GL_INVALID_VALUE:
    return "GL_INVALID_VALUE";
  case GL_INVALID_OPERATION:
    return "GL_INVALID_OPERATION";
  case GL_INVALID_FRAMEBUFFER_OPERATION:
    return "GL_INVALID_FRAMEBUFFER_OPERATION";
  case GL_OUT_OF_MEMORY:
    return "GL_OUT_OF_MEMORY";
  case GL_STACK_UNDERFLOW:
    return "GL_STACK_UNDERFLOW";
  case GL_STACK_OVERFLOW:
    return "GL_STACK_OVERFLOW";
  }
  return NULL;
}

const char* glerror_get_error_description(GLenum error)
{
  switch(error){
  case GL_NO_ERROR:
    return "No error has been recorded. The value of this symbolic "
      "constant is guaranteed to be 0.";

  case GL_INVALID_ENUM:
    return "An unacceptable value is specified for an enumerated "
      "argument. The offending command is ignored and has no "
      "other side effect than to set the error flag.";

  case GL_INVALID_VALUE:
    return "A numeric argument is out of range. The offending "
      "command is ignored and has no other side effect than "
      "to set the error flag.";

  case GL_INVALID_OPERATION:
    return "The specified operation is not allowed in the current "
      "state. The offending command is ignored and has no "
      "other side effect than to set the error flag.";

  case GL_INVALID_FRAMEBUFFER_OPERATION:
    return "The framebuffer object is not complete. The offending "
      "command is ignored and has no other side effect than "
      "to set the error flag.";

  case GL_OUT_OF_MEMORY:
    return "There is not enough memory left to execute the "
      "command. The state of the GL is undefined, except for "
      "the state of the error flags, after this error is "
      "recorded.";

  case GL_STACK_UNDERFLOW:
    return "An attempt has been made to perform an operation that "
      "would cause an internal stack to underflow.";

  case GL_STACK_OVERFLOW:
    return "An attempt has been made to perform an operation that "
      "would cause an internal stack to overflow.";
  }

  return NULL;
}
