#include "graphics/meshconv.h"

#include "util/log.h"

static vec3s vec3_compute_normal(vec3s const a, vec3s const b, vec3s const c)
{
  return glms_vec3_cross(glms_vec3_sub(b, a), glms_vec3_sub(c, a));
}

static void mesh_compute_normal_and_update(
  mesh_vertex_t* vertices, size_t a, size_t b, size_t c)
{
  mesh_vertex_t* av = &vertices[a];
  mesh_vertex_t* bv = &vertices[b];
  mesh_vertex_t* cv = &vertices[c];
  av->normal = bv->normal = cv->normal = vec3_compute_normal(
      av->vertex,
      bv->vertex,
      cv->vertex
  );
}

static void obj_add_component_to_mesh(
  obj_format_t const* obj,
  obj_face_desc_t const* fdesc,
  int** curr_fi,
  mesh_vertex_t** vertices)
{

  int32_t next_index = *((*curr_fi)++) - 1;
  obj_vertex_t* ov = &obj->vertices[next_index];
  mesh_vertex_t mv;
  mv.vertex.x = ov->x;
  mv.vertex.y = ov->y;
  mv.vertex.z = ov->z;

  if(fdesc->components & OBJ_HAS_TEXTURE)
    (*curr_fi)++;

  if(fdesc->components & OBJ_HAS_NORMAL){
    next_index = *((*curr_fi)++) - 1;
    obj_normal_t* on = &obj->normals[next_index];
    mv.normal.x = on->x;
    mv.normal.y = on->y;
    mv.normal.z = on->z;
  }

  *vertices = dyna_add(*vertices, &mv);
}

void obj_to_mesh(obj_format_t const* obj, mesh_t* mesh)
{
  mesh_vertex_t* vertices = dyna_create(mesh_vertex_t, 8);

  int* curr_fi = obj->face_indices;
  size_t const n_faces = dyna_size(obj->face_descs);
  for(size_t i=0; i<n_faces; ++i){
    obj_face_desc_t* fdesc = &obj->face_descs[i];
    size_t has_texture = (fdesc->components&OBJ_HAS_TEXTURE) > 0;
    size_t has_normal = (fdesc->components&OBJ_HAS_NORMAL) > 0;
    size_t face_size = fdesc->n_indices / (1 + has_texture + has_normal);

    if(face_size < 3 || face_size > 4){
      log_err("faces with size < 3 or > 4 are not supported!\n");
      return;
    }

    if(face_size == 3){
      for(size_t j=0; j<3; ++j){
        obj_add_component_to_mesh(obj, fdesc, &curr_fi, &vertices);
      }

      if(!has_normal){
        size_t n = dyna_size(vertices);
        mesh_compute_normal_and_update(vertices, n - 3, n - 2, n - 1);
      }
    }else{
      // for faces of size 4, we have to add as two triangles
      // indices 0 1 2, as one triangle
      // then 2 3 0, as another triangle

      for(size_t j=0; j<3; ++j)
        obj_add_component_to_mesh(obj, fdesc, &curr_fi, &vertices);

      size_t n = dyna_size(vertices);
      mesh_vertex_t* last = dyna_ptr_at(vertices, n - 1);
      mesh_vertex_t* first = dyna_ptr_at(vertices, n - 3);

      vertices = dyna_add(vertices, last);
      obj_add_component_to_mesh(obj, fdesc, &curr_fi, &vertices);
      vertices = dyna_add(vertices, first);

      if(!has_normal){
        n = dyna_size(vertices);
        mesh_compute_normal_and_update(vertices, n - 6, n - 5, n - 4);
        mesh_compute_normal_and_update(vertices, n - 3, n - 2, n - 1);
      }
    }
  }

  mesh_create(mesh, vertices, dyna_size(vertices));
  dyna_destroy(vertices);
}

/**
  * converts the description of a stl file to a mesh in GPU
  *
  * @param stl stl to be converted
  * @param mesh an initialized mesh object
  */
void stl_to_mesh(stl_format_t const* stl, mesh_t* mesh)
{
  size_t n_triangles = dyna_size(stl->triangles);
  size_t n_vertices = n_triangles * 3;
  mesh_vertex_t* vertices = (mesh_vertex_t*) malloc(
    sizeof(mesh_vertex_t) * n_vertices
  );

  for(uint32_t i=0, vidx=0; i<n_triangles; ++i){
    for(uint32_t j=0; j<3; ++j, ++vidx){
      vertices[vidx].vertex.x = stl->triangles[i].vertex[j][0];
      vertices[vidx].vertex.y = stl->triangles[i].vertex[j][1];
      vertices[vidx].vertex.z = stl->triangles[i].vertex[j][2];

      vertices[vidx].normal.x = stl->triangles[i].normal[0];
      vertices[vidx].normal.y = stl->triangles[i].normal[1];
      vertices[vidx].normal.z = stl->triangles[i].normal[2];
    }
  }

  mesh_create(mesh, vertices, n_vertices);
  free(vertices);
}
