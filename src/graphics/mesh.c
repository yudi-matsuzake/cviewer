#include "graphics/mesh.h"
#include "util/log.h"

#include <string.h>

/**
  * "upload" the mesh to gpu
  */
static void upload_mesh(mesh_t* m)
{
  // setup buffers and vbos
  mesh_bind(m);
  glBufferData(
    GL_ARRAY_BUFFER,
    (long) sizeof(mesh_vertex_t) * m->count,
    m->vertices,
    GL_STATIC_DRAW);

  m->color = (vec3s){{ 1.f, 1.f, 1.f }};
}

void mesh_create(mesh_t* m, mesh_vertex_t const* vertices, size_t count)
{
  size_t mesh_sz = sizeof(mesh_vertex_t) * count;
  m->vertices = (mesh_vertex_t*) malloc(mesh_sz);
  m->count = (GLsizei) count;
  m->model = (mat4s) GLMS_MAT4_IDENTITY_INIT;

  memcpy(m->vertices, vertices, mesh_sz);

  // create buffers and vbos
  glGenVertexArrays(1, &m->vao);
  glGenBuffers(1, &m->vbo);

  /*
   * set vertex array layout
   */
  glBindVertexArray(m->vao);
  glBindBuffer(GL_ARRAY_BUFFER, m->vbo);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  // normal attribute
  glVertexAttribPointer(
    1, 3, GL_FLOAT, GL_FALSE,
    6 * sizeof(float),
    (void*)(3 * sizeof(float))
  );
  glEnableVertexAttribArray(1);

  upload_mesh(m);
}

void mesh_destroy(mesh_t const* m)
{
  free(m->vertices);
  glDeleteVertexArrays(1, &m->vao);
  glDeleteBuffers(1, &m->vbo);
}

void mesh_bind(mesh_t const* m)
{
  GL(glBindVertexArray(m->vao));
  GL(glBindBuffer(GL_ARRAY_BUFFER, m->vbo));
}

void mesh_draw(mesh_t const* m)
{
  mesh_bind(m);
  GL(glDrawArrays(GL_TRIANGLES, 0, m->count));
}
