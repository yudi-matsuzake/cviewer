#ifndef __GRID_H__
#define __GRID_H__

#include "graphics/shaders.h"

/**
  * @file grid.h
  * this file implements and describes a simple 3D grid
  */

typedef struct{
  vec4s color;
  shader_t shader;
  GLuint vao;
  GLuint vbo;
} grid_render_t;

void grid_render_create(grid_render_t* g);

/**
  * @param g grid render struct
  * @param clip_size the size of the lines in the grid
  * @param projection projection matrix
  * @param view matrix
  */
void grid_render_draw(
  grid_render_t const* g,
  float clip_size,
  mat4s const* projection,
  mat4s const* view);

void grid_render_destroy(grid_render_t const* g);

#endif
