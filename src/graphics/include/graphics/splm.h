#ifndef __SPLM_H__
#define __SPLM_H__

#include "graphics/scene.h"
#include "graphics/shaders.h"

/**
  * @file splm.h
  * this file implements and describes a simple phong lighting model
  * implemented with GLSL
  */

typedef struct{
  shader_t shader;
  float ambient_light_strength;
} splm_t;

void splm_create(splm_t* p);

/**
  * use the phong shader to draw objects
  */
void splm_use(
  splm_t const* p,
  scene_t const* scene,
  mat4s const* projection,
  mat4s const* view,
  mat4s const* model,
  vec3s const* color);

void splm_destroy(splm_t* p);

#endif
