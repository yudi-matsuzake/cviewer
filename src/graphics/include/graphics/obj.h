#ifndef __OBJ_H__
#define __OBJ_H__

#include <stdint.h>
#include <stdio.h>

/**
  *@file obj.h describes and defines helper to load a obj file
  *
  * the function was implemented making no explicit assumptions of the file
  * generation at all, for instance, a common assumption is that faces are
  * 3 or 4 in size or that all vertices are declared always before than the normals
  *
  * I think that such assumptions are sane, but not there is no guaranteed
  * that is always the case
  *
  * some optimization are possible making such assumptions
  * but I prefer this to be more generic and always work
  * but compromising performance
  *
  * I also prefer to not load all the file in the memory, and reading always as
  * a stream of data
  *
  * TODO: do not support materials, parameter space vertices (free form geometry), 
  * smooth shading
  */

typedef struct{
  float x, y, z, w;
} obj_vertex_t;

typedef struct{
  float u, v, w;
} obj_texture_coord_t;

typedef struct{
  float x, y, z;
} obj_normal_t;

typedef enum {
  OBJ_HAS_ONLY_VERTEX = 0,
  OBJ_HAS_NORMAL  = 1,
  OBJ_HAS_TEXTURE = 1 << 1
} obj_face_components_t;

typedef struct{
  size_t n_indices;
  obj_face_components_t components;
} obj_face_desc_t;

typedef struct{
  obj_vertex_t* vertices;
  obj_texture_coord_t* textures;
  obj_normal_t* normals;
  int* face_indices;
  obj_face_desc_t* face_descs;
} obj_format_t;

/**
  * allocates memory for `triangle_count` in a previous allocated struct
  * `obj`
  */
void obj_create(obj_format_t* obj);

/*
 * destroy the object `obj`
 */
void obj_destroy(obj_format_t const* obj);

/**
  * write in the `out` file the obj file format described by `obj`
  * with binary description if `binary` > 0
  */
void obj_write(obj_format_t const* obj, FILE* out, int binary);

/**
  * read the `in` obj file and populates `obj`
  *
  * if the file could not be read or understand, the obj->vertices
  * would be null after this functions returns
  *
  * @return 0 if failed 1 if success
  *
  * @see obj_destroy
  */
int obj_read(obj_format_t* obj, FILE* in);

#endif
