#ifndef __MESH_H__
#define __MESH_H__

#include <stddef.h>

#include "cglm/struct.h"

#include "graphics/opengl.h"

/** @file mesh.h
  * definition of a mesh of unstructured triangles in the RAM and 
  * in the GPU
  */

typedef struct{
  vec3s vertex;
  vec3s normal;
}mesh_vertex_t;

/**
  * hold the data from a mesh that exists either in the RAM
  * as in GPU memory
  */
typedef struct{
  GLuint vao, vbo;
  mat4s model;

  vec3s color;

  mesh_vertex_t* vertices;
  GLsizei count;
}mesh_t;

/**
  * creates a mesh and upload to gpu with `count` vertices described by
  * following the pointer `vertices`
  *
  * `vertices` data could be safely freed after calling `mesh_create`
  *
  * @param m mesh struct to be populated
  * @param vertices vertices to be created in the gpu
  * @param count number of vertices in `vertices`
  */
void mesh_create(mesh_t* m, mesh_vertex_t const* vertices, size_t count);

/**
  * destroys all resources created by `m` in gpu and in memory
  */
void mesh_destroy(mesh_t const* m);

/**
  * bind the mesh and the buffer to be draw
  */
void mesh_bind(mesh_t const* m);

/**
  * updates the vertices in ram to the gpu
  */
void mesh_update(mesh_t const* m);

/**
  * draws the mesh
  */
void mesh_draw(mesh_t const* m);

#endif
