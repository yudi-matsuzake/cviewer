#ifndef __LIGHT_H__
#define __LIGHT_H__

#include "cglm/struct.h"

typedef struct{
  vec3s position;
  vec3s color;
}light_t;

#endif
