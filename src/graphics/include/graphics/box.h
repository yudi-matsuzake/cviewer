#ifndef __BOX_H__
#define __BOX_H__

#include "cglm/struct.h"

typedef struct{
  vec3s position;
  vec3s sides;
}box_t;

#endif
