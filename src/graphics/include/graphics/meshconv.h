#ifndef __MESHCONV_H__
#define __MESHCONV_H__

/**
  * @file meshconv.h
  * 
  * this file defines and implements mesh conversions, for instance, obj description
  * to mesh or stl -> mesh
  */

#include "graphics/mesh.h"
#include "graphics/obj.h"
#include "graphics/stl.h"

/**
  * converts the description of a obj file to a mesh in GPU
  *
  * @param obj obj to be converted
  * @param mesh an initialized mesh object
  */
void obj_to_mesh(obj_format_t const* obj, mesh_t* mesh);


/**
  * converts the description of a stl file to a mesh in GPU
  *
  * @param stl stl to be converted
  * @param mesh an initialized mesh object
  */
void stl_to_mesh(stl_format_t const* stl, mesh_t* mesh);

#endif
