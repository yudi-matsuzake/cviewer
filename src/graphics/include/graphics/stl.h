#ifndef __STL_H__
#define __STL_H__

#include <stdio.h>
#include <stdint.h>

#include "util/dyna.h"

/**
  *@file stl.h descrives and defines helper to load a STL file
  */

typedef struct{
  float normal[3];
  float vertex[3][3];
} stl_triangle_t;

typedef struct{
  stl_triangle_t* triangles;
} stl_format_t;

/**
  * allocates memory for `triangle_count` in a previous allocated struct
  * `stl`
  */
void stl_create(stl_format_t* stl, uint32_t triangle_count);

/*
 * destroy the object `stl`
 */
void stl_destroy(stl_format_t const* stl);

/**
  * write in the `out` file the stl file format described by `stl`
  * with binary description if `binary` > 0
  */
void stl_write(stl_format_t const* stl, FILE* out, int binary);

/**
  * read the `in` stl file and populates `stl`
  *
  * if the file could not be read or understand, the stl->vertices
  * would be null after this functions returns
  *
  * @see stl_destroy
  */
void stl_read(stl_format_t* stl, FILE* in);

#endif
