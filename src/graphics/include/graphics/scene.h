#ifndef __SCENE_H__
#define __SCENE_H__

#include <stdbool.h>

#include "graphics/camera.h"
#include "graphics/light.h"
#include "graphics/mesh.h"


/**
  * additional mesh info
  */
typedef struct{
  char const* filename;
  bool enabled;
} mesh_info_t;

typedef struct{
  light_t light;
  camera_t camera;
  mesh_t* meshes;
  mesh_info_t* mesh_info;
}scene_t;

#endif
