#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "cglm/struct.h"

/**@file camera.h
  * this file describes a simple camera interface
  * to help with simple camera management
  */

typedef struct{
  vec3s position;
  vec3s direction;
}camera_t;

/**
  * computes the right axis of the camera
  */
vec3s camera_compute_right_axis(camera_t const* c);

/**
  * computes the up axis of the camera given the right axis
  */
vec3s camera_compute_up_axis_given(camera_t const* c, vec3s right);

/**
  * computes the up axis of the camera
  */
vec3s camera_compute_up_axis(camera_t const* c);

/**
  * computes the view matrix (lookat matrix) described by `c`
  * @param c camera object
  */
mat4s camera_view_matrix(camera_t const* c);

/**
  * translates `c` with the offset `t` based on the
  * base axis described by `c` direction
  *
  * @param c camera object
  * @param t offset vector
  */
void camera_translate(camera_t* c, vec3s t);

/**
  * aim the camera `c` to the target `p`
  *
  * @param c camera object
  * @param p point to aim at
  */
void camera_aim_to(camera_t* c, vec3s p);

#endif
