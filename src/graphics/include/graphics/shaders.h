#ifndef __SHADER_H__
#define __SHADER_H__

#include <stddef.h>
#include "cglm/struct.h"
#include "graphics/opengl.h"

/**
  * this files describes and instantiates opengl shader programs 
  */

typedef union ivec2s {
  int raw[2];
  struct {
    int x;
    int y;
  };
} ivec2s;

/**
  * description of a list of source code used by opengl
  */
typedef struct{
  char const** sources;
  GLsizei count;
}shader_source_t;

typedef GLuint shader_t;

/**
  * create a shader and stores it in `s`
  *
  * @param s shader object description to be populated
  * @param vertex_shader vertex shader source code
  * @param fragment_shader fragment shader source code
  */
shader_t shader_create(
    shader_source_t const* vertex_shader,
    shader_source_t const* fragment_shader);

/**
  * destroys the shader object, freeing all resources
  * from the GPU
  */
void shader_destroy(shader_t s);

/**
  * use the shader program to draw primitives
  */
void shader_use(shader_t s);

/**
  * set the uniform with name `name` in the shader `s` with the
  * value `v`
  *
  * @param s shader program
  * @param name name of the uniform to be modified
  * @param v vec2 variable 
  */
void shader_set_uniform_vec2(shader_t s, char const* name, vec2s const* v);

/**
  * set the uniform with name `name` in the shader `s` with the
  * value `v`
  *
  * @param s shader program
  * @param name name of the uniform to be modified
  * @param v vec3 variable 
  */
void shader_set_uniform_vec3(shader_t s, char const* name, vec3s const* v);

/**
  * set the uniform with name `name` in the shader `s` with the
  * value `v`
  *
  * @param s shader program
  * @param name name of the uniform to be modified
  * @param v vec4 variable 
  */
void shader_set_uniform_vec4(shader_t s, char const* name, vec4s const* v);

/**
  * set the uniform with name `name` in the shader `s` with the
  * value `m`
  *
  * @param s shader program
  * @param name name of the uniform to be modified
  * @param v mat4
  */
void shader_set_uniform_mat4(shader_t s, char const* name, mat4s const* m);

/**
  * set the uniform with name `name` in the shader `s` with the
  * value `v`
  *
  * @param s shader program
  * @param name name of the uniform to be modified
  * @param v scalar
  */
void shader_set_uniform_float(shader_t s, char const* name, float v);

/**
  * set the uniform with name `name` in the shader `s` with the
  * value `v`
  *
  * @param s shader program
  * @param name name of the uniform to be modified
  * @param v scalar
  */
void shader_set_uniform_ivec2(shader_t s, char const* name, ivec2s const* v);

#endif
