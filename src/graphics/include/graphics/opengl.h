/** @file this file includes graphic and window context
 * for the project; also, gives helper functions and macros
 * to debug simple computer graphics applications
 */

#ifndef __OPENGL_H__
#define __OPENGL_H__

// WARN: include glad.h before glfw3.h
#include "glad/glad.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

void glerror_msg(
  int pre,
  const char* call,
  const char* file,
  long line,
  const char* function,
  const char* error_macro,
  const char* error_description);

void glerror_pre_callback(
  const char* call,
  const char* file,
  long line,
  const char* function,
  const char* error_macro,
  const char* error_description);

void glerror_post_callback(
  const char* call,
  const char* file,
  long line,
  const char* function,
  const char* error_macro,
  const char* error_description);

const char* glerror_get_error_macro(GLenum error);

const char* glerror_get_error_description(GLenum error);

#define GL_TEST(x, file, line, pretty_function, callback)  \
  while((error = glGetError()) != GL_NO_ERROR){    \
    callback(          \
      #x,          \
      file,          \
      line,          \
      pretty_function,      \
      glerror_get_error_macro(error),  \
      glerror_get_error_description(error)\
    );            \
  }

#ifdef NDEBUG
#define GL_CALL(x, file, line, pretty_function) x
#else
#define GL_CALL(x, file, line, pretty_function)    \
  do{              \
    GLenum error;          \
    GL_TEST(          \
      x,          \
      file,          \
      line,          \
      pretty_function,      \
      glerror_pre_callback  \
    );            \
    x;            \
    GL_TEST(          \
      x,          \
      file,          \
      line,          \
      pretty_function,      \
      glerror_post_callback  \
    );            \
  }while(0)
#endif

#define GL(x) GL_CALL(x, __FILE__, __LINE__, __func__)

#endif
