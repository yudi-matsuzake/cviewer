#include "graphics/gridrender.h"

static const char* grid_render_vs =
"#version 330 core\n"

"uniform mat4 u_view;\n"
"uniform mat4 u_projection;\n"

"layout (location = 0) in vec3 p;\n"
"layout (location = 1) in vec4 l_color;\n"

"out vec4 color;\n"

"void main()\n"
"{\n"
"  gl_Position = u_projection*u_view*vec4(p, 1.f);\n"
"  color = l_color;\n"
"}";

static const char* grid_render_fs =
"#version 330 core\n"

"out vec4 fragment_color;\n"
"in vec4 color;\n"

"void main()\n"
"{\n"
"  fragment_color = color;\n"
"}\n";

void grid_render_create(grid_render_t* g)
{
  g->shader = shader_create(
    &((shader_source_t){ &grid_render_vs, 1 }),
    &((shader_source_t){ &grid_render_fs, 1 })
  );
  g->color = (vec4s){{ .5f, .5f, .5f, 1.f }};
  GL(glGenVertexArrays(1, &g->vao));
  GL(glGenBuffers(1, &g->vbo));
}

void grid_render_destroy(grid_render_t const* g)
{
  shader_destroy(g->shader);
  GL(glDeleteVertexArrays(1, &g->vao));
  GL(glDeleteBuffers(1, &g->vbo));
}

void grid_render_draw(
  grid_render_t const* g,
  float clip_size,
  mat4s const* projection,
  mat4s const* view)
{
  GL(glUseProgram(g->shader));
  GL(glEnable(GL_LINE_SMOOTH));
  GL(glLineWidth(1.f));
  GL(glBindVertexArray(g->vao));
  GL(glBindBuffer(GL_ARRAY_BUFFER, g->vbo));

  GL(glVertexAttribPointer(
    0, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(float), (void*)0));
  GL(glEnableVertexAttribArray(0));
  GL(glVertexAttribPointer(
    1, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(float), (void*)(sizeof(float) * 3)));
  GL(glEnableVertexAttribArray(1));

  float l = clip_size;
  float lines[] = {
    -l, 0, 0,  1.f, 0.f, 0.f, 0.5f,
    l, 0, 0,   1.f, 0.f, 0.f, 0.5f,
    0, -l, 0,  0.f, 1.f, 0.f, 0.5f,
    0, l, 0,   0.f, 1.f, 0.f, 0.5f,
    0, 0, -l,  0.f, 0.f, 1.f, 0.5f,
    0, 0, l,   0.f, 0.f, 1.f, 0.5f
  };
  GL(glBufferData(GL_ARRAY_BUFFER, sizeof(lines), lines, GL_DYNAMIC_DRAW));

  shader_set_uniform_mat4(g->shader, "u_view", view);
  shader_set_uniform_mat4(g->shader, "u_projection", projection);

  GL(glDrawArrays(GL_LINES, 0, 2));
  GL(glDrawArrays(GL_LINES, 2, 2));
  GL(glDrawArrays(GL_LINES, 4, 2));
}
