#include "graphics/camera.h"
#include "cglm/struct/vec3.h"


vec3s camera_compute_right_axis(camera_t const* c)
{
  vec3s const up = { {0.f, 1.f, 0.f} };
  vec3s const cross = glms_cross(c->direction, up);
  if(cross.x == .0f && cross.y == .0f && cross.z == .0f)
    return (vec3s){{ -1.f, .0f, .0f }};

  return glms_normalize(cross);
}

vec3s camera_compute_up_axis_given(camera_t const* c, vec3s right)
{
  return glms_normalize(glms_cross(right, c->direction));
}

vec3s camera_compute_up_axis(camera_t const* c)
{
  return camera_compute_up_axis_given(c, camera_compute_right_axis(c));
}

mat4s camera_view_matrix(camera_t const* c)
{
  return glms_lookat(
    c->position,
    glms_vec3_add(c->position, c->direction),
    camera_compute_up_axis(c)
  );
}

void camera_translate(camera_t* c, vec3s t)
{
  vec3s const right = camera_compute_right_axis(c);
  vec3s const up    = camera_compute_up_axis_given(c, right);

	// c->position += t.x * right + t.y * up + t.z * -c->direction;
  c->position = glms_vec3_add(
    c->position,
    glms_vec3_add(
      glms_vec3_add(glms_vec3_scale(right, t.x), glms_vec3_scale(up, t.y)),
      glms_vec3_scale(glms_vec3_negate(c->direction), t.z)
    )
  );
}

void camera_aim_to(camera_t* c, vec3s p)
{
  c->direction = glms_normalize(glms_vec3_sub(p, c->position));
}
