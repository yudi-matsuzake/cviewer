#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>

#include "cglm/struct.h"
#define CIMGUI_DEFINE_ENUMS_AND_STRUCTS
#include "cimgui.h"
#include "generator/output/cimgui_impl.h"

#include "util/util.h"
#include "util/dyna.h"
#include "util/log.h"
#include "graphics/opengl.h"
#include "graphics/shaders.h"
#include "graphics/scene.h"
#include "graphics/splm.h"
#include "graphics/stl.h"
#include "graphics/obj.h"
#include "graphics/meshconv.h"
#include "graphics/box.h"
#include "graphics/gridrender.h"

typedef struct{
  vec3s move;
  vec3s axis_look;
  vec2s look;
  bool exit;
  bool rotate_look;
  bool look_origin;
  bool look_bb_center;
  bool toggle_ui;
}input_data_t;

static input_data_t new_input = {
  .move = {{ 0.f, 0.f, 0.f }},
  .axis_look = {{ 0.f, 0.f, 0.f }},
  .look = {{ 0.f, 0.f }},
  .exit = false,
  .rotate_look = false,
  .look_origin = false,
  .look_bb_center = false,
  .toggle_ui = false
};

typedef struct{
  float camera_velocity;
  float scroll_velocity;
  float mouse_sensitivity;

  bool grid_enabled;
} app_configuration_t;

typedef struct{
  app_configuration_t configuration;

  GLFWwindow* window;
  int width, height;
  float delta;
  float fps;

  ImGuiContext* imgui_context;
  ImGuiIO* imgui_io;

  box_t bouding_box;

  int ui_enabled;
  scene_t scene;
}app_t;

typedef struct{
  int* keys;
  int* mods;
  int action;
}key_input_t;

const struct{
  key_input_t exit,
    foward,
    backward,
    up,
    down,
    left,
    right,
    rotate_look,
    look_origin,
    look_bb_center,
    toggle_ui,
    look_x,
    look_y,
    look_z,
    look_nx,
    look_ny,
    look_nz;
}key_map = {
  .exit = {
    (int[]){GLFW_KEY_ESCAPE, GLFW_KEY_Q, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .foward = {
    (int[]){GLFW_KEY_W, GLFW_KEY_UP, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .backward = {
    (int[]){GLFW_KEY_S, GLFW_KEY_DOWN, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .up = {
    (int[]){GLFW_KEY_W, GLFW_KEY_UP, GLFW_KEY_UNKNOWN},
    (int[]){GLFW_KEY_LEFT_SHIFT, GLFW_KEY_RIGHT_SHIFT, GLFW_KEY_UNKNOWN},
    GLFW_PRESS
  },
  .down = {
    (int[]){GLFW_KEY_S, GLFW_KEY_DOWN, GLFW_KEY_UNKNOWN},
    (int[]){GLFW_KEY_LEFT_SHIFT, GLFW_KEY_RIGHT_SHIFT, GLFW_KEY_UNKNOWN},
    GLFW_PRESS
  },
  .left = {
    (int[]){GLFW_KEY_A, GLFW_KEY_LEFT, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .right = {
    (int[]){GLFW_KEY_D, GLFW_KEY_RIGHT, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .rotate_look = {
    NULL,
    (int[]){GLFW_KEY_LEFT_CONTROL, GLFW_KEY_RIGHT_CONTROL, GLFW_KEY_UNKNOWN},
    GLFW_PRESS
  },
  .look_origin = {
    (int[]){GLFW_KEY_PERIOD, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .look_bb_center = {
    (int[]){GLFW_KEY_COMMA, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .toggle_ui = {
    (int[]){GLFW_KEY_M, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .look_x = {
    (int[]){GLFW_KEY_X, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .look_y = {
    (int[]){GLFW_KEY_Y, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .look_z = {
    (int[]){GLFW_KEY_Z, GLFW_KEY_UNKNOWN},
    NULL,
    GLFW_PRESS
  },
  .look_nx = {
    (int[]){GLFW_KEY_X, GLFW_KEY_UNKNOWN},
    (int[]){GLFW_KEY_LEFT_SHIFT, GLFW_KEY_RIGHT_SHIFT, GLFW_KEY_UNKNOWN},
    GLFW_PRESS
  },
  .look_ny = {
    (int[]){GLFW_KEY_Y, GLFW_KEY_UNKNOWN},
    (int[]){GLFW_KEY_LEFT_SHIFT, GLFW_KEY_RIGHT_SHIFT, GLFW_KEY_UNKNOWN},
    GLFW_PRESS
  },
  .look_nz = {
    (int[]){GLFW_KEY_Z, GLFW_KEY_UNKNOWN},
    (int[]){GLFW_KEY_LEFT_SHIFT, GLFW_KEY_RIGHT_SHIFT, GLFW_KEY_UNKNOWN},
    GLFW_PRESS
  }
};

static void error_callback(int error, const char* description)
{
  log_err("glfw error (%d): %s\n", error, description);
}

int scroll_input(int set, double* p_x, double* p_y)
{
  static int has_to_consume = 0;
  static double x, y;

  if(set){
    has_to_consume = 1;
    x = *p_x;
    y = *p_y;

    return 1;
  }

  if(has_to_consume){
    has_to_consume = 0;
    *p_x = x;
    *p_y = y;
    return 1;
  }

  return 0;
}

static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
  (void)window;
  scroll_input(1, &xoffset, &yoffset);
}

static bool get_mouse_movement(app_t const* app, vec2s* mov)
{
  static double last_mx, last_my;
  static int is_m_valid = 0;

  if(glfwGetMouseButton(app->window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS){
    double cx, cy;
    glfwGetCursorPos(app->window, &cx, &cy);

    if(is_m_valid){
      *mov = (vec2s){{
        (float) (cx - last_mx),
        (float) (cy - last_my)
      }};

      last_mx = cx;
      last_my = cy;

      return true;
    }

    last_mx = cx;
    last_my = cy;

    is_m_valid = 1;
    return false;
  }

  is_m_valid = 0;
  return false;
}

static int is_activated(app_t const* app, int const* ks, int action)
{
  for(int const* k = ks; *k != GLFW_KEY_UNKNOWN; ++k)
    if(glfwGetKey(app->window, *k) == action)
      return 1;
  return 0;
}

static bool key_input_is_activated(app_t const* app, key_input_t const* ki)
{
  return (ki->mods == NULL || is_activated(app, ki->mods, ki->action))
    && (ki->keys == NULL || is_activated(app, ki->keys, ki->action));
}

static input_data_t process_input(app_t* app)
{
  input_data_t input = new_input;

  // process input
  if(key_input_is_activated(app, &key_map.exit))
      input.exit = 1;

  // camera movement
  if(key_input_is_activated(app, &key_map.up)){
    input.move = glms_vec3_add(input.move, (vec3s){{ .0f, +1.f, 0.f }});
  }else if(key_input_is_activated(app, &key_map.foward)){
    input.move = glms_vec3_add(input.move, (vec3s){{ .0f, .0f, -1.f }});
  }

  if(key_input_is_activated(app, &key_map.down)){
    input.move = glms_vec3_add(input.move, (vec3s){{ .0f, -1.f, 0.f }});
  }else if(key_input_is_activated(app, &key_map.backward)){
    input.move = glms_vec3_add(input.move, (vec3s){{ .0f, .0f, +1.f }});
  }

  if(key_input_is_activated(app, &key_map.left))
    input.move = glms_vec3_add(input.move, (vec3s){{ -1.0f, .0f, 0.f }});

  if(key_input_is_activated(app, &key_map.right))
    input.move = glms_vec3_add(input.move, (vec3s){{ +1.0f, .0f, 0.f }});

  // look origin
  input.look_origin = key_input_is_activated(app, &key_map.look_origin);

  // look origin
  if(key_input_is_activated(app, &key_map.look_bb_center))
    input.look_bb_center = true;

  // toggle ui
  static int have_toggle = 0;
  if(key_input_is_activated(app, &key_map.toggle_ui)){
    if(!have_toggle)
      input.toggle_ui = true;
    have_toggle = 1;
  }else{
    have_toggle = 0;
  }

  // scroll
  double sx = 0., sy = 0.;
  scroll_input(0, &sx, &sy);
  input.move = glms_vec3_add(
      input.move,
      glms_vec3_scale(
        (vec3s){{ .0f, .0f, (float)sy }},
        app->configuration.scroll_velocity
      )
  );

  // setup data
  get_mouse_movement(app, &input.look);
  input.look = glms_vec2_scale(input.look, app->configuration.mouse_sensitivity);
  input.rotate_look = key_input_is_activated(app, &key_map.rotate_look);

  // look from axis
  if(key_input_is_activated(app, &key_map.look_nx)){
    input.axis_look.x = -1;
  }else if(key_input_is_activated(app, &key_map.look_x)){
    input.axis_look.x = +1;
  }

  if(key_input_is_activated(app, &key_map.look_ny)){
    input.axis_look.y = -1;
  }else if(key_input_is_activated(app, &key_map.look_y)){
    input.axis_look.y = +1;
  }

  if(key_input_is_activated(app, &key_map.look_nz)){
    input.axis_look.z = -1;
  }else if(key_input_is_activated(app, &key_map.look_z)){
    input.axis_look.z = +1;
  }

  return input;
}

static void scene_look_axis(
  scene_t* s, box_t const* bouding_box, vec3s const* axis_look)
{

  vec3s const half_sides = glms_vec3_scale(bouding_box->sides, 1.f/2.f);
  vec3s const bb_center = glms_vec3_add(bouding_box->position, half_sides);

  if(dyna_size(s->meshes) > 0
      && (axis_look->x != 0.f || axis_look->y != 0.f || axis_look->z != 0.f)){

    float const min_dist = glms_vec3_max(half_sides) * 3.f;
    float const dist = glms_vec3_norm(s->camera.position);

    s->camera.position = glms_vec3_add(
      bb_center,
      glms_vec3_scale(
        glms_vec3_normalize(*axis_look),
        MAX(dist, min_dist)
      )
    );

    camera_aim_to(&s->camera, bb_center);
  }
}

static float radians(float angle)
{
  return angle *  (float) M_PI / 180.f;
}

static void update(app_t* app, input_data_t const* input)
{
  if(input->exit)
    glfwSetWindowShouldClose(app->window, true);

  if(input->toggle_ui)
    app->ui_enabled = !app->ui_enabled;

  float scene_size_factor = glms_vec3_max(app->bouding_box.sides);

  float const cam_velocity = app->configuration.camera_velocity
    * scene_size_factor;

  camera_translate(
    &app->scene.camera,
    glms_vec3_scale(input->move, app->delta*cam_velocity)
  );

  int w, h;
  glfwGetWindowSize(app->window, &w, &h);

  if(input->look.x != 0.f || input->look.y != 0.f){

    float yaw = radians(input->look.x);
    float pitch = radians(input->look.y);

    if(input->rotate_look){

      vec3s const half_sides = glms_vec3_scale(app->bouding_box.sides, 1.f/2.f);
      vec3s const bb_center = glms_vec3_add(
          app->bouding_box.position,
          half_sides
      );

      app->scene.camera.position = glms_vec3_sub(
          app->scene.camera.position,
          bb_center
      );

      app->scene.camera.position = glms_vec3_rotate(
        glms_vec3_rotate(
          app->scene.camera.position,
          yaw,
          (vec3s){{ .0f, 1.f, .0f }}
        ),
        pitch,
        (vec3s){{ 1.f, 0.f, .0f }}
      );

      app->scene.camera.position = glms_vec3_add(
          app->scene.camera.position,
          bb_center
      );

      camera_aim_to(&app->scene.camera, bb_center);

    }else{
      vec3s const right = camera_compute_right_axis(&app->scene.camera);
      vec3s const up = camera_compute_up_axis_given(&app->scene.camera, right);
      app->scene.camera.direction = glms_vec3_rotate(
        glms_vec3_rotate(app->scene.camera.direction, (float) yaw, up),
        (float) pitch,
        right
      );
    }
  }

  if(input->look_origin)
    camera_aim_to(&app->scene.camera, (vec3s){{ 0.f, 0.f, 0.f }});

  vec3s const half_sides = glms_vec3_scale(app->bouding_box.sides, 1.f/2.f);
  vec3s const bb_center = glms_vec3_add(
      app->bouding_box.position,
      half_sides
  );

  if(input->look_bb_center)
    camera_aim_to(&app->scene.camera, bb_center);

  scene_look_axis(&app->scene, &app->bouding_box, &input->axis_look);
}

GLFWwindow* window_create_and_setup()
{
  // setup and create window
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(1280, 720, "cviewer", NULL, NULL);
  if (!window)
    fatal_error("Could not inicialize glfw window\n");

  glfwMakeContextCurrent(window);
  gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
  glfwSwapInterval(1);

  glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);
  glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, GLFW_TRUE);

  // set callbacks
  /* glfwSetKeyCallback(window, key_callback); */
  glfwSetErrorCallback(error_callback);
  glfwSetScrollCallback(window, scroll_callback);

  return window;
}

void opengl_initial_setup()
{
  GL(glEnable(GL_DEPTH_TEST));
  GL(glEnable(GL_BLEND));
  GL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
  GL(glClearColor(.0f, .0f, .0f, 1.f));
}

void imgui_init(app_t* app)
{
  // IMGUI_CHECKVERSION();
  app->imgui_context = igCreateContext(NULL);
  app->imgui_io = igGetIO();

  const char* glsl_version = "#version 330 core";
  ImGui_ImplGlfw_InitForOpenGL(app->window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  // Setup style
  igStyleColorsDark(NULL);

  // set misc configurations for imgui
  app->imgui_io->ConfigFlags = ImGuiConfigFlags_NavEnableKeyboard;
}

void imgui_terminate(ImGuiContext* c)
{
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  igDestroyContext(c);
}

void imgui_update(app_t* app)
{
  if(!app->ui_enabled) return;

  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  igNewFrame();

  if(!igBegin("cview", NULL, ImGuiWindowFlags_NoBringToFrontOnFocus)){
    igEnd();
    return;
  }

  igText("welcome to cviewer!");
  if(igCollapsingHeader_TreeNodeFlags("help", 0)){
    float const indent = 20.f;

    igSeparator();
    igText("USAGE: cviewer [file.obj|file.stl] ...");
    igSeparator();
    igText("CONTROLLERS:");
    igBulletText("misc:");

    igIndent(indent);
    igBulletText("toggle ui:                 M");
    igUnindent(indent);

    igBulletText("movement:");
    igIndent(indent);
    igBulletText("foward:                    W|UP|SCROLL");
    igBulletText("backward:                  S|DOWN");
    igBulletText("left:                      A|LEFT");
    igBulletText("right:                     D|RIGHT");
    igBulletText("up:                        SHIFT + W|UP");
    igBulletText("down:                      SHIFT + S|DOWN");
    igBulletText(
      "rotate around object:                 SHIFT + LEFT_CLICK + mouse movement"
    );
    igUnindent(indent);

    igBulletText("camera look around:");
    igIndent(indent);
    igBulletText("look around:               LEFT_CLICK + mouse movement");
    igBulletText("aim to origin:             .");
    igBulletText("aim to bouding box center: ,");
    igBulletText("look from the +x axis:     X");
    igBulletText("look from the +y axis:     Y");
    igBulletText("look from the +z axis:     Z");
    igBulletText("look from the -x axis:     SHIFT + X");
    igBulletText("look from the -y axis:     SHIFT + Y");
    igBulletText("look from the -z axis:     SHIFT + Z");
    igUnindent(indent);

    igSeparator();
  }

  if(igCollapsingHeader_TreeNodeFlags("configuration", 0)){
    igInputFloat(
      "camera movement velocity",
      &app->configuration.camera_velocity,
      app->configuration.camera_velocity * 0.1f,
      app->configuration.camera_velocity * 0.1f,
      "%f",
      0
    );

    igInputFloat(
      "scroll velocity",
      &app->configuration.scroll_velocity,
      app->configuration.scroll_velocity * 0.1f,
      app->configuration.scroll_velocity * 0.1f,
      "%f",
      0
    );

    igInputFloat(
      "mouse sensitivity",
      &app->configuration.mouse_sensitivity,
      app->configuration.mouse_sensitivity * 0.1f,
      app->configuration.mouse_sensitivity * 0.1f,
      "%f",
      0
    );

    igCheckbox("grid enabled", &app->configuration.grid_enabled);
  }

  if(igCollapsingHeader_TreeNodeFlags("status", 0)){
    igText("fps: %f", (double) (1.f/app->delta));
    igSeparator();
  }

  if(igCollapsingHeader_TreeNodeFlags("scene", 0)){
    igInputFloat3("camera position", app->scene.camera.position.raw, "%f", 0);

    static vec3s aim = {{ .0f, .0f, .0f }};
    igInputFloat3("", aim.raw, "%f", 0);
    igSameLine(0, 0);
    if(igButton("aim to", (ImVec2){ 0, 0 })){
      camera_aim_to(&app->scene.camera, aim);
    }

    igInputFloat3("light position", app->scene.light.position.raw, "%f", 0);
    igColorEdit3("light color", app->scene.light.color.raw, 0);

    if (igTreeNode_Str("meshes")){
      for(size_t i=0; i<dyna_size(app->scene.meshes); ++i){

        mesh_info_t* info = &app->scene.mesh_info[i];

        igSeparator();
        if(igTreeNode_Str(info->filename)){
          igCheckbox("enabled", &info->enabled);
          if(info->enabled){
            mesh_t* m = &app->scene.meshes[i];

            igBulletText("%d vertices", m->count);
            igBulletText("%d triangles", m->count / 3);

            igColorEdit3("color", m->color.raw, 0);
            igSeparator();
          }
          igTreePop();
        }
      }

      igTreePop();
    }
  }

  igEnd();
  /* igShowDemoWindow(NULL); */
}

void imgui_render(app_t* app)
{
  if(app->ui_enabled){
    igRender();
    ImGui_ImplOpenGL3_RenderDrawData(igGetDrawData());
  }
}

static int load_mesh_from_stl(char const* filepath, mesh_t* m)
{

  FILE* f = fopen(filepath, "r");
  stl_format_t stl;
  stl_read(&stl, f);

  if(f == NULL){
    log_err("could not open file [%s]!\n", filepath);
    return 0;
  }

  if(stl.triangles == NULL){
    log_err("i do not understand the file [%s] as a stl file!\n", filepath);
    return 0;
  }

  log_inf("file %s has %zu triangles\n", filepath, dyna_size(stl.triangles));

  stl_to_mesh(&stl, m);
  log_inf(
    "mesh derived from %s has %u vertices\n",
    filepath,
    m->count
  );

  stl_destroy(&stl);
  fclose(f);

  return 1;
}

static int load_mesh_from_obj(char const* filepath, mesh_t* m)
{

  FILE* f = fopen(filepath, "r");
  if(f == NULL){
    log_err("could not open file [%s]!\n", filepath);
    return 0;
  }

  obj_format_t obj;
  obj_read(&obj, f);

  if(obj.vertices == NULL
    || obj.normals == NULL
    || obj.face_indices == NULL
    || obj.face_descs == NULL){

    log_err("i do not understand the file [%s] as a obj file!\n", filepath);
    return 0;
  }

  log_inf("file %s has %zu faces\n", filepath, dyna_size(obj.face_descs));

  obj_to_mesh(&obj, m);
  log_inf(
    "mesh derived from %s has %u vertices\n",
    filepath,
    m->count
  );

  obj_destroy(&obj);
  fclose(f);

  return 1;
}

box_t compute_scene_bounding_box(scene_t const* s)
{
  vec3s min = {{INFINITY, INFINITY, INFINITY}};
  vec3s max = {{-INFINITY, -INFINITY, -INFINITY}};

  for(size_t i=0; i<dyna_size(s->meshes); ++i){
    for(int j=0; j<s->meshes[i].count; ++j){
      mesh_vertex_t const* v = &s->meshes[i].vertices[j];

      if(v->vertex.x < min.x) min.x = v->vertex.x;
      if(v->vertex.y < min.y) min.y = v->vertex.y;
      if(v->vertex.z < min.z) min.z = v->vertex.z;
      if(v->vertex.x > max.x) max.x = v->vertex.x;
      if(v->vertex.y > max.y) max.y = v->vertex.y;
      if(v->vertex.z > max.z) max.z = v->vertex.z;
    }
  }

  return (box_t){ .position = min, .sides = glms_vec3_sub(max, min) };
}

static vec3s random_color()
{
  return (vec3s){{ 
    (float) rand() / (float) RAND_MAX,
    (float) rand() / (float) RAND_MAX,
    (float) rand() / (float) RAND_MAX
  }};
}

static void scene_add_mesh(scene_t* s, mesh_t* m, char const* file)
{
  m->color = random_color();
  s->meshes = dyna_add(s->meshes, m);
  mesh_info_t info = { .filename = file, .enabled = true };
  s->mesh_info = dyna_add(s->mesh_info, &info);
}

/*
 * create a scene from a list of files
 */
void scene_create(scene_t* s, box_t* bb, char** files, int n_files)
{

  s->meshes = dyna_create(mesh_t, 0);
  s->mesh_info = dyna_create(mesh_info_t, 0);

  for(int i=0; i<n_files; ++i){
    log_inf("loading file %s\n", files[i]);

    if(endswith(files[i], ".stl")){
      log_inf("loading as stl file!\n");

      mesh_t m;
      if(load_mesh_from_stl(files[i], &m))
        scene_add_mesh(s, &m, files[i]);

    }else if(endswith(files[i], ".obj")){
      log_inf("loading as obj file!\n");

      mesh_t m;
      if(load_mesh_from_obj(files[i], &m))
        scene_add_mesh(s, &m, files[i]);

    }else{
      log_err("unknown file type of [%s]\n", files[i]);
    }
  }

  if(dyna_size(s->meshes)){
    *bb = compute_scene_bounding_box(s);

    vec3s const look_axis = {{ 1.f, 1.f, 1.f }};
    scene_look_axis(s, bb, &look_axis);

    s->light = (light_t) {
      .position = glms_vec3_scale(glms_vec3_add(bb->position, bb->sides), 3.f),
      .color = {{1.f, 1.f, 1.f}}
    };
  }else{
    s->camera = (camera_t){
      .position = {{ .0f, .0f, -1.f }},
      .direction = {{ .0f, .0f, 1.f }}
    };
    s->light = (light_t) { 
      .position = {{ 1.f, 1.f, 1.f }},
      .color = {{1.f, 1.f, 1.f}}
    };
  }

}

void scene_destroy(scene_t const* s)
{
  for(size_t i=0; i<dyna_size(s->meshes); ++i)
    mesh_destroy(&s->meshes[i]);
  dyna_destroy(s->meshes);
  dyna_destroy(s->mesh_info);
}

int main(int argc, char* argv[])
{
  log_inf("initializing glfw\n");
  if (!glfwInit())
    fatal_error("Could not inicialize glfw");

  app_t app;
  app.configuration = (app_configuration_t){
      .camera_velocity = 1.f,
      .scroll_velocity = 5.f,
      .mouse_sensitivity = .1f,
      .grid_enabled = true,
  };

  app.window = window_create_and_setup();
  opengl_initial_setup();

  app.ui_enabled = 1;
  imgui_init(&app);
  scene_create(&app.scene, &app.bouding_box, argv + 1, argc - 1);

  log_inf("create a simple phong lighting model\n");
  splm_t phong;
  splm_create(&phong);

  log_inf("create grid render\n");
  grid_render_t grid_render;
  grid_render_create(&grid_render);

  // main loop
  float last_time = (float) glfwGetTime();
  while(!glfwWindowShouldClose(app.window)){

    // setup render
    float const time = (float) glfwGetTime();
    app.delta = time - last_time;
    last_time = time;
    glfwGetFramebufferSize(app.window, &app.width, &app.height);

    // process input and update state
    glfwPollEvents();

    input_data_t input = process_input(&app);
    if((!app.imgui_io->WantCaptureMouse && !app.imgui_io->WantCaptureKeyboard))
      update(&app, &input);

    // update ui
    imgui_update(&app);

    // compute mpv matrices
    float clip_size = glms_vec3_max(app.bouding_box.sides) * 10.f;
    mat4s const projection = glms_perspective(
        (float) M_PI / 4.f,
        (float) app.width / (float) app.height,
        0.1f,
        clip_size
    );

    mat4s const view = camera_view_matrix(&app.scene.camera);

    // render
    GL(glViewport(0, 0, app.width, app.height));
    GL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    if(app.configuration.grid_enabled)
      grid_render_draw(&grid_render, clip_size, &projection, &view);

    for(size_t i=0; i<dyna_size(app.scene.meshes); ++i){
      if(app.scene.mesh_info[i].enabled){
        mesh_t const* m = &app.scene.meshes[i];
        splm_use(&phong, &app.scene, &projection, &view, &m->model, &m->color);
        mesh_draw(m);
      }
    }

    // render ui
    imgui_render(&app);

    // swap buffers
    glfwSwapBuffers(app.window);
  }

  scene_destroy(&app.scene);
  grid_render_destroy(&grid_render);
  splm_destroy(&phong);

  imgui_terminate(app.imgui_context);
  glfwDestroyWindow(app.window);

  glfwTerminate();

  return EXIT_SUCCESS;
}
