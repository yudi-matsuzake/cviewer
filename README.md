# cviewer

cviewer is a simple 3D mesh viewer, currently supporting .stl and .obj

## dependencies

* [glfw](https://www.glfw.org)

* [cmake](https://cmake.org/)

## compilation

1. Clone the repository and the git submodules:

```
git clone --recursive git@gitlab.com:yudi-matsuzake/cviewer.git
```

2. generate the project and compile:

```
cd cviewer
cmake -B build-release -DCMAKE_BUILD_TYPE=Release
make -C build-release
```

## USAGE

```
USAGE: cviewer [file.stl | file.obj] ...
```
